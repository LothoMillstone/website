#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Door;
use Mojo::Base 'Mojolicious::Plugin';

use Carp 'croak';
use Mojo::IOLoop::Subprocess;
use Mojo::Promise;
use Mojo::Util qw(dumper unquote);
use Time::Piece;

our $VERSION = '0.1';

has 'host';
has 'token';
has 'uuid';

sub register {
  my ($self, $app, $config) = @_;

  $self->host($config->{host} // 'ballarathackerspace.org.au');
  $self->uuid($config->{uuid} // 'bad');
  $self->token($config->{token} // 'bad');

  $app->log->info(sprintf("Door configured with uuid/token at %s: %s/%s", $self->host, $self->uuid, $self->token));

  $app->helper('door.enter' => sub {
    my $uuid = $self->uuid;
    my $token = $self->token;
    my $host = $self->host;

    my $promise = Mojo::Promise->new;
    my $subprocess = Mojo::IOLoop::Subprocess->new;

    $subprocess->run(sub {
      my $cmd = "mosquitto_pub -h $host -u $uuid -P $token -I mosqpub_door_ -t monitor/door -m 1";

      $app->log->debug("CMD: $cmd");
      system($cmd);
      return $? == 0;
    }, sub {
      my ($sp, $err, @results) = @_;

      $promise->reject(0) if $err;
      $promise->resolve($results[0]);
    });

    return $promise;
  });
}

1;
