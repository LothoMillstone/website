# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

use FindBin;
use lib "$FindBin::Bin/lib";

use Test::More;

use Mojo::MQTT::Message;

# test decode and encode of a "published" message
my $hex = "301e001773747265616d732f696f742d74656d70657261747572653133393031";
my $bytes = pack "H*", $hex;

my $m = Mojo::MQTT::Message->new_from_bytes($bytes);
my $m_bytes = $m->encode;
my $m_hex = unpack "H*", $m_bytes;

is($m_hex, $hex, 'right answer');

done_testing();
