# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

use FindBin;
use lib "$FindBin::Bin/lib";

use Test::More;
use Test::Exception;
use Time::Piece;
use Mojo::File 'path';
use Mojo::Util 'dumper';

use Test::Mojo;

my $script = path(__FILE__)->dirname->sibling('bhackd');

# no config set, use default
dies_ok { Test::Mojo->new($script); } 'right result';

# now with test config
$ENV{BHACKD_CONFIG} = 't/test.conf';
$t = Test::Mojo->new($script);

# add device
ok(!$t->app->devices->add, 'right result');

# test device entries
my $devices = [
  {
    name  => 'foo',
    uuid  => '1',
    token => 'a',
    email => 'foo@bar.com',
  },
  {
    name => 'bar',
    uuid => '2',
    token => 'b',
    email => 'foo@baz.com',
  },
  {
    name => 'bar',
    uuid => '3',
    token => 'c',
    email => 'foo@baz.com',
    roles => 32768,
  },
  {
    name => 'baz',
    uuid => '3',
    token => 'c',
    email => 'foo@bar.com',
  },
  {
    name => 'daz',
    uuid => '4',
    token => 'd',
    email => 'daz@bar.com',
    roles => 32768,
    meta => {
      here => 'there'
    }
  }
];

# adding
ok($t->app->devices->add($devices->[0]), 'right result');
ok($t->app->devices->add($devices->[1]), 'right result');
ok($t->app->devices->add($devices->[2]), 'right result');
ok(!$t->app->devices->add($devices->[3]), 'right result');
ok($t->app->devices->add($devices->[4]), 'right result');

# exists
ok($t->app->devices->exists('2'), 'right result');
ok($t->app->devices->exists('4'), 'right result');

# find devices
my $found;
$found = $t->app->devices->find;
is(@{$found->{items}}, 4, 'right result');

$found = $t->app->devices->find(fetch => 2);
is(@{$found->{items}}, 2, 'right result');

$found = $t->app->devices->find({uuid => '1'});
is(@{$found->{items}}, 1, 'right result');
is($found->{items}[0]->{name}, 'foo', 'right result');
is($found->{items}[0]->{used}, 0, 'right result');

$found = $t->app->devices->find(name => 'bar');
is(@{$found->{items}}, 2, 'right result');

$found = $t->app->devices->find(email => 'foo@baz.com');
is(@{$found->{items}}, 2, 'right result');
is($found->{items}[1]->{name}, 'bar', 'right result');
is($found->{items}[1]->{uuid}, '3', 'right result');

my $relative = gmtime->epoch . '.5';
$found = $t->app->devices->find(relative => $relative);
is(@{$found->{items}}, 0, 'right result');

# update devices
ok(!$t->app->devices->update({}), 'right result');
ok(!$t->app->devices->update({uuid => 4}), 'right result');
ok(!$t->app->devices->update(uuid => 4, token => 'd'), 'right result');
ok(!$t->app->devices->update(uuid => 4, token => 'd'), 'right result');
ok(!$t->app->devices->update(uuid => 4, token => 'd', email => 'new@new.com'), 'right result');
ok($t->app->devices->update(uuid => 4, token => 'd', email => 'new@new.com', name => 'wow'), 'right result');
ok($t->app->devices->update(uuid => 4, token => 'd', email => 'new@new.com', name => 'wow', roles => 32768, meta => {}), 'right result');

# check super users
ok(!$t->app->devices->is_super, 'right result');
ok(!$t->app->devices->is_super({uuid => 1}), 'right result');
ok($t->app->devices->is_super(uuid => 4), 'right result');

# validate devices
ok($t->app->devices->validate('1', 'a'), 'right result');
ok(!$t->app->devices->validate('2', 'a'), 'right result');
ok($t->app->devices->validate('2', 'b'), 'right result');
ok(!$t->app->devices->validate(undef, 'a'), 'right result');
ok(!$t->app->devices->validate('1', undef), 'right result');

# bump validation used a second time and check
ok($t->app->devices->validate('1', 'a'), 'right result');
$found = $t->app->devices->find({uuid => '1'});
is($found->{items}[0]->{used}, 2, 'right result');

# delete devices
ok(!$t->app->devices->delete, 'right result');
ok($t->app->devices->delete({email => 'foo@bar.com'}), 'right result');
ok($t->app->devices->delete(uuid => 2), 'right result');

$found = $t->app->devices->find;
is(@{$found->{items}}, 2, 'right result');
is($found->{items}[0]->{uuid}, '3', 'right result');

done_testing();
