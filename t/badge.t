# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

use FindBin;
use lib "$FindBin::Bin/lib";

use Test::More;
use Mojo::File 'path';

use Test::Mojo;

my $script = path(__FILE__)->dirname->sibling('bhackd');

$ENV{BHACKD_CONFIG} = 't/test.conf';

my $t = Test::Mojo->new($script);

$t->app->log->level('fatal');

# add badge
ok(!$t->app->badges->add, 'right result');

my $badges = [
  {
    title => 'foo',
    image_url => 'http://foo.com/foo.png',
    category => 'a',
  },
  {
    title => 'bar',
    image_url => 'http://bar.com/bar.gif',
    category => 'b',
  },
  {
    title => 'baz',
    image_url => 'http://baz.com/baz.jpg',
    category => 'a',
  }
];

ok($t->app->badges->add($badges->[0]), 'right result');
ok($t->app->badges->add($badges->[1]), 'right result');
ok($t->app->badges->add($badges->[2]), 'right result');


# find badge
my $found;
$found = $t->app->badges->find;
is(@{$found->{items}}, 3, 'right result');

$found = $t->app->badges->find(fetch => 2);
is(@{$found->{items}}, 2, 'right result');

$found = $t->app->badges->find(title => 'foo');
is(@{$found->{items}}, 1, 'right result');
is($found->{items}[0]->{title}, 'foo', 'right result');

$found = $t->app->badges->find(category => 'a');
is(@{$found->{items}}, 2, 'right result');

$found = $t->app->badges->find(relative => '0.0');
is(@{$found->{items}}, 0, 'right result');


# update badge
ok(!$t->app->badges->update(id => 3), 'right result');
ok(!$t->app->badges->update(title => 'woot'), 'right result');
ok($t->app->badges->update(title => 'bat', id => 3), 'right result');


# delete badge
ok(!$t->app->badges->delete, 'right result');
ok($t->app->badges->delete(title => 'foo'), 'right result');
ok($t->app->badges->delete(id => 2), 'right result');

$found = $t->app->badges->find;
is(@{$found->{items}}, 1, 'right result');
is($found->{items}[0]->{title}, 'bat', 'right result');

done_testing();
