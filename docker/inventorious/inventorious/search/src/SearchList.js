import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
// import SearchField from 'react-search-field';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import SearchBar from 'material-ui-search-bar'
// import Button from '@material-ui/core/Button';
// import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/core/styles';
// import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import 'typeface-roboto';
// List Items
import Drawer from '@material-ui/core/Drawer';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
//import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import BuildIcon from '@material-ui/icons/Build';
import BuildOutlineIcon from '@material-ui/icons/BuildOutlined';
import Badge from '@material-ui/core/Badge';
//import TemporaryDrawer from './SideBarDetail'
import placeholder from './placeholder.jpg';
import Image from 'material-ui-image';
import Fade from '@material-ui/core/Fade';
// const dotenv = require('dotenv');


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 600,
    backgroundColor: theme.palette.background.paper,
  },
});

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}


function BuildIconStock(props) {
  const CurrentIcon = ((props.stockLevel).toString() !== "0" ) ? <BuildIcon/> : <BuildOutlineIcon/>;
  return(CurrentIcon);
}


class BadgeVisibility extends React.Component {
  state = {
    invisible: false,
  };
}


function SimpleList(props) {
  return (
        <ListItemLink button data-key={props.key} href="#simple-list" onClick={(e) => (TemporaryDrawer) => this.toggleDrawer.bind('right', true, props.dataKey) }>
            <Badge color="secondary" badgeContent={ props.stockLevel } >
                <BuildIconStock stockLevel={props.stockLevel} />
            </Badge>
            <ListItemText primary={props.itemName} secondary={props.locationSimple} />
        </ListItemLink>
  );
}

SimpleList.propTypes = {
  classes: PropTypes.object.isRequired,
};

class SearchList extends Component {
  constructor(props) {
      super(props);  

      this.state = {
        dataSource: [],
        items: [],
        error: null,
        isLoaded: false,
        searchTerm: 'placehold',
        drawer: false,
        drawerContent: ''
      }
  }

  componentDidMount() {
    document.title = "Inventorious - for glory!";
    fetch( process.env.REACT_APP_PARTS_URL )
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            dataSource: result,
            items: result // Show all on startup
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }  

  handleUpdate = (value) => {

    // If we don't want to search unless there is enough data.
    if (value.length < 0) { return false; }
    let processedList = this.state.dataSource.filter(
    function(result) {
        return result.name.toLowerCase().includes(value.toLowerCase());
    }).sort();

   this.setState({items: processedList});

   document.title = `Inventorious - search for: ${value}`

  // console.log(processedList);
  }

  DrawerImage(image) {
    const { error, isLoaded, items, drawerId } = this.state;
    // Import result is the URL of your image
    // return (


    // <img src={placeholder} alt="Part picture" 
    // style={{flex:1, height: undefined, width: undefined}}
    // />
    // )
    return (
      <div>
        <Image
        // src="http://loremflickr.com/200/150"
        src={image}
        />
      </div>
    )
  }

  moreInfo(details) {
   let info = Object.entries(details);
      return (
        <div>
          {
            info.map((item, index) => (
            <li key={index}>{item[0]}: {item[1]}</li>
            ))
          }
        </div>
      );
    }    
    
  

   DrawerRenderer() {
    const { error, isLoaded, items, drawerId } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Climbing the Himalayas...</div>;
    } 
    
    const { classes } = this.props;

    if (drawerId) {
    const item = items.find(function(element) {
      return element.id === drawerId;
    })


    if (item !== null) {
    let description = item.name ? item.name : 'no results found';

    let locationSimple = item.location;
    let stockLevel = item.stockLevel; //Math.floor(Math.random() * 4); // we have it with item.stockLevel but lets make it look fun for demonstration
    let outOfStock = (item.stockLevel.toString() !== "0");
    let moreInfo = item.hasOwnProperty('moreInfo');
    let moreContent = null
    let image = item.hasOwnProperty('imageURL') ? item.imageURL : placeholder

    // const sideList = <div style={{'minWidth':'200px'}} >{ this.DrawerRenderer() }</div>;
    if (moreInfo) {
      // moreContenty = "<h6>More details</h6>";
      // console.log(Array.from(item.moreInfo));
      // ( Object.entries(item.moreInfo) ).forEach(function (item, index) {
      //   // console.log(`We're looking into showing ${index}`);
      //   // console.log(item);
      //   if (typeof(item[1]) === "string" ) {
      //     moreContent = moreContent.concat( `<li>hi ${item[0]}: ${item[1]}</li>` );
      //   }
      // });
      moreContent = this.moreInfo( item.moreInfo )  
      
    }


      return (
        <div className={classes.list} style={{'margin':'1rem','minWidth':'100px'}}>
        <Typography variant="h4" color="inherit">
                {description}
              </Typography>
                <div color="primary">Ballarat hackerspace</div>
              <Divider/>
              <ul>
              <li>
                Stock: {stockLevel}
                {
                outOfStock ? null : (                
                  <span>  - oops, Out of stock! <span role="img" aria-label="sad face">😔</span> </span>
                  )
                }
              </li>
                <li>Location: {locationSimple}</li>
              </ul>
              <div style={{
                margin: "0 auto",
                minWidth:"300px",
                maxWidth:"350px"}}> 
                { this.DrawerImage(image) }
              </div>
              <ul className="moreInfo" >{moreContent}</ul>

              <Divider/>
              {/* <button>I'd like more of this!</button> */}
        </div> 
      );
  } else { // No matches found, fill in the drawer with a placeholder
    return(
      <div className={classes.list} style={{'margin':'1rem','minWidth':'200px'}}>
      <Typography variant="h4" color="inherit">
              Inventorious
            </Typography>
            <div>Still waiting!</div>
            <Divider/>
            <p>Stock: x</p>
            {/* <button>I'd like more of this!</button> */}
      </div> 
      ); 
    }
}
    
  }
  
  toggleDrawer = (open, id) => () => {
    // Set Message:
    this.setState({
      drawer: open,
      drawerId: id
    });
  };

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Polarising ice caps...</div>;
    }



    
    const results = items.map((item, id) => {
      const description = item.name ? item.name : 'no results found';
      const locationSimple = item.location;
      const stockLevel = item.stockLevel;
      const globalId = item.id;

      // return (
      //   <SimpleList 
      //       key={item.id}
      //       itemName={description}
      //       locationSimple={locationSimple}
      //       stockLevel={stockLevel}
      //       dataKey={item.id}
      //     />
      // );
//(e) => (TemporaryDrawer) => this.toggleDrawer.bind('right', true, props.dataKey)
      return(
        <Fade in={true} key={item.id}>
          <ListItemLink button key={item.id} data-key={item.id} href="#" onClick={this.toggleDrawer(true, item.id ) } >
          <Badge color="secondary" badgeContent={ item.stockLevel } >
              <BuildIconStock stockLevel={item.stockLevel} />
          </Badge>
          <ListItemText primary={item.name} secondary={item.location} />
          </ListItemLink>
        </Fade>
      )


    })
    
    const sideList = <div style={{'minWidth':'200px'}} >{ this.DrawerRenderer() }</div>;
  
    return (
      
      <MuiThemeProvider>
      <SearchBar
        onChange={(value) => this.handleUpdate(value)}
        onRequestSearch={(value) => this.handleUpdate(value)}
        style={{
          margin: "0 auto",
          marginTop:".75em",
          maxWidth: 800
        }}
      />
      <Grid container spacing={24}
      justify = "center"
      >
      
        <Grid item xs={12} lg={10}>
        <div>
          <List component="nav">
            {results}
          </List>
        </div>
        </Grid>
      </Grid>
      <SwipeableDrawer
          anchor="right"
          open={this.state.drawer}
          onClose={this.toggleDrawer(false, this.state.drawerId)}
        >
          <div
            tabIndex={0}
            role="button"
            onKeyDown={this.toggleDrawer(false, this.state.drawerId)}
          >
          
          </div>
          { sideList }
        </SwipeableDrawer>

    </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(SearchList);