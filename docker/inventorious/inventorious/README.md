# Inventorious
Why should you have everything else in your hackerspace be glorious - get Inventorious! a minimalist parts and inventory system.
It can pair up with JSON-Server when you are using it locally, a sqlite3 based database serving out an API, or anything which returns a loosely formatted JSON array.

Screens:
![Search](https://i.imgur.com/vvwuTIK.jpg), [in vertical](https://imgur.com/IWeg4ks)
![View Details](https://i.imgur.com/OZI9uIZ.jpg),  
![Add](https://i.imgur.com/kGWNl8g.jpg) 
![Adding a location](https://i.imgur.com/1vy8n6M.jpg)

Browse the search folder for more information.
I plan to add a dashboard and real API hosting at the root location. 

