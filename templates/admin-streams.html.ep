% layout 'page', active_page => 'admin', page_title => 'Admin Streams';

<div ng-controller="AdminStreamsController as adminStreamsVm">
  <section class="section">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="title text-center"><a href="/admin">Admin</a> <i class="fa fa-angle-double-right"></i> Streams</h2>
          <p>Manage the Ballarat Hackerspace streams.</p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#streams" aria-controls="streams" role="tab" data-toggle="tab">Streams</a></li>
            <li role="presentation"><a href="#acls" aria-controls="acls" role="tab" data-toggle="tab">ACLs</a></li>
          </ul>
          <span class="label-title pull-right" bh-tooltip="Real-time Status"><i class="fa fa-fw" ng-class="{'fa-exclamation-circle fa-red': !adminStreamsVm.isConnected(), 'fa-check-circle fa-green': adminStreamsVm.isConnected()}"></i> {{adminStreamsVm.formatConnectedState()}}</span>

          <!-- tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="streams">
              <button class="btn btn-cta btn-cta-red pull-right" ng-click="adminStreamsVm.deleteStream({stream: '*'})">Delete All Streams</button>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Stream</th>
                    <th>Points</th>
                    <th>Created</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="item in adminStreamsVm.streams">
                    <td><a href="/meta/streams/{{item.stream}}">{{item.stream}}</a></td>
                    <td>{{item.points}}</td>
                    <td><bh-time epoch="{{item.timestamp}}" /></td>
                    <td class="text-right">
                      <button class="btn btn-cta btn-cta-sm btn-cta-red" ng-click="adminStreamsVm.deleteStream(item)"><i class="fa fa-fw fa-times"></i></button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="acls">
              <button class="btn btn-cta btn-cta-primary pull-right" ng-click="adminStreamsVm.addAcl()"><i class="fa fa-fw fa-plus"></i> Add ACL</button>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>UUID</th>
                    <th>Topic</th>
                    <th class="text-center">Access</th>
                    <th>Created</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="item in adminStreamsVm.streamsAcls | orderBy:'uuid'">
                    <td>{{item.uuid}}</td>
                    <td>{{item.topic}}</td>
                    <td class="text-center">{{adminStreamsVm.formatAclAccess(item)}}</td>
                    <td><bh-time epoch="{{item.timestamp}}" /></td>
                    <td class="text-right">
                      <button class="btn btn-cta btn-cta-sm btn-cta-blue" ng-click="adminStreamsVm.editAcl(item)"><i class="fa fa-fw fa-edit"></i></button>
                      <button class="btn btn-cta btn-cta-sm btn-cta-red" ng-click="adminStreamsVm.deleteAcl(item)"><i class="fa fa-fw fa-times"></i></button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- acl create modal -->
<script type="text/ng-template" id="modalAclCreate.html">
  <div class="modal-header">
    <h4 class="modal-title">ACL <i class="fa fa-angle-right"></i> Add</h4>
  </div>
  <div class="modal-body">
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-tag"></i></span>
        <input type="text" class="form-control" placeholder="UUID" ng-model="modalVm.acl.uuid">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-list-ul"></i></span>
        <input type="text" class="form-control" placeholder="Topic" ng-model="modalVm.acl.topic">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-key"></i></span>
        <select type="number" class="form-control" min="0" max="3" placeholder="Access" ng-model="modalVm.acl.access">
          <option value="1">Read Only</option>
          <option value="2">Write Only</option>
          <option value="3">Read/Write</option>
        </select>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-cta pull-left" type="button" ng-click="modalVm.cancel()">Cancel</button>
    <button class="btn btn-cta btn-cta-primary" type="button" ng-click="modalVm.ok()" ng-disabled="!modalVm.canAdd()"><i class="fa fa-fw fa-plus"></i> Add</button>
  </div>
</script>

<!-- acl update modal -->
<script type="text/ng-template" id="modalAclEdit.html">
  <div class="modal-header">
    <h4 class="modal-title">ACL <i class="fa fa-angle-right"></i> Edit</h4>
  </div>
  <div class="modal-body">
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-tag"></i></span>
        <input type="text" class="form-control" placeholder="UUID" ng-model="modalVm.acl.uuid">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-list-ul"></i></span>
        <input type="text" class="form-control" placeholder="Topic" ng-model="modalVm.acl.topic">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-key"></i></span>
        <select type="number" class="form-control" min="0" max="3" placeholder="Access" ng-model="modalVm.acl.access">
          <option value="1">Read Only</option>
          <option value="2">Write Only</option>
          <option value="3">Read/Write</option>
        </select>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-cta pull-left" type="button" ng-click="modalVm.cancel()">Cancel</button>
    <button class="btn btn-cta btn-cta-green" type="button" ng-click="modalVm.ok()" ng-disabled="!modalVm.canSave()"><i class="fa fa-fw fa-save"></i> Save</button>
  </div>
</script>

<!-- acl delete modal -->
<script type="text/ng-template" id="modalAclDelete.html">
  <div class="modal-header">
    <h4 class="modal-title">ACL <i class="fa fa-angle-right"></i> Delete</h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to remove the following ACL:</p>
    <ul>
      <li>UUID: {{modalVm.acl.uuid}}</li>
      <li>Topic: {{modalVm.acl.topic}}</li>
      <li>Access: {{modalVm.acl.access}}</li>
    </ul>
  </div>
  <div class="modal-footer">
    <button class="btn btn-cta" type="button" ng-click="modalVm.cancel()">Cancel</button>
    <button class="btn btn-cta btn-cta-red pull-left" type="button" ng-click="modalVm.ok()">Yes, Delete</button>
  </div>
</script>

<!-- stream delete all modal -->
<script type="text/ng-template" id="modalStreamDeleteAll.html">
  <div class="modal-header">
    <h4 class="modal-title">Stream <i class="fa fa-angle-right"></i> Delete</h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete all current streams?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-cta" type="button" ng-click="modalVm.cancel()">Cancel</button>
    <button class="btn btn-cta btn-cta-red pull-left" type="button" ng-click="modalVm.ok()">Yes, Delete All</button>
  </div>
</script>

<!-- stream delete modal -->
<script type="text/ng-template" id="modalStreamDelete.html">
  <div class="modal-header">
    <h4 class="modal-title">Stream <i class="fa fa-angle-right"></i> Delete</h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to remove the following stream:</p>
    <ul><li>{{modalVm.stream.stream}}</li></ul>
  </div>
  <div class="modal-footer">
    <button class="btn btn-cta" type="button" ng-click="modalVm.cancel()">Cancel</button>
    <button class="btn btn-cta btn-cta-red pull-left" type="button" ng-click="modalVm.ok()">Yes, Delete</button>
  </div>
</script>

<script>
% use Mojo::JSON 'encode_json';
% use Mojo::Util 'decode';
  window.bhack={
    streams:<%== decode 'UTF-8', encode_json $streams %>,
    streamsAcls:<%== decode 'UTF-8', encode_json $streams_acls %>,
  };
</script>
