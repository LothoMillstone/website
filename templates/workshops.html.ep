% layout 'page', active_page => 'workshops', page_title => 'Workshops';

<!-- ******team Section****** -->
<div ng-controller="WorkshopController as workshopVm">
  <section id="workshops" class="section">
    <div class="container">
      <h1 class="title text-center">Workshops</h1>

  % if (flash 'info') {
      <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p><%= flash 'info' %></p>
      </div>
  % } elsif (flash 'error') {
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p><%= flash 'error' %></p>
      </div>
  % }
      <!-- Hey future hackerspace-member-changing-this-page, don't forget to "unchange" this page after the workshop - go on, set a calendar reminder, right now, I'll wait... -->
      <p>
        Our workshops are developed by our expert members covering a range of topics that relate to science, technology, engineering, arts and maths. We run them at a near-cost price to encourage the community to learn new skills and increase accessibility.
      </p>

      <h2> Up next - Fighting Robots! </h2>

      <p>Build your own fighting robot with our themed night and test your skills at the upcoming Robot Fight Night!</p>

      <div class="center">
      <img src="/images/workshops/robot.jpg" style="
          width:  60%;
          min-width: 200px;
          padding: 0.1em;
          margin-bottom: 0.5em;
          margin-top: 0.5em;
          border: 2px solid black;
          background-color: darkgrey;
          display: block;
      " class="img-center">
      </div>

      <p>The Ballarat Area Robot League (BARL) is having their next <b>Robot Fight Night</b> coming up November 29th. To celebrate, and to get you into the competition, we are having a themed night to build a fighting robot base. Check out more information at <a href="https://barl.io">barl.io</a>.</p>
      <ul>
        <li>BYO parts, or come chat to work out what to buy</li>
        <li>get help to put your bot together,</li>
        <li>meet a community of like-minded members,</li>
        <li>test against other bots in battle, and</li>
        <li>work on upgrades and "version 2.0".</li>
      </ul>

      <p>The theme night is a community build style, where people bring their own materials and work next to each other, helping. </p>

      <p><b>Tuesday 19th November, 6pm - 9pm</b></p>
      <p>Come any time during this window. You'll need to be a Ballarat Hackerspace member to use any equipment or parts in the space.</p>

      <p>If you need accessible access, please contact us separately at committee@ballarathackerspace.org.au for us to arrange this. We have a lift, but it is locked to the public by default.</p>

      <p>Under 18s are welcome, but <b>must</b> be with a guardian over 18 who is responsible for them at all times. All participants will need to sign a waiver before completing the course.</p>

      <h2> More Workshops </h2>
      <p>If you are looking for something else, our standard range of workshops includings the following:</p>

      <!-- No really, go set that calendar reminder for after the workshop (or when registrations close) -->

      <ul>
        <li><b>Internet of Things (IoT)</b>, turning things on and off via the internet, dealing with data streams and connecting devices to the internet.</li>
        <li><b>Build your own 3D Printer</b>, built from standard components, giving a very deep understanding of how these machines work.</li>
        <li><b>Rasperry Pi 101</b>, an introductory workshop on programming, electornics and hacking that is very accessible to all.</li>
        <li><b>Build your own Drone/Quadcopter</b>, for building your own drone and learning to fly.</li>
        <li><b>Internet of Things 101</b>, connecting equipment to the internet and controlling devices over any distance.</li>
        <li><b>Arduino 101</b>, learning about programming integrated circuits.</li>
      </ul>

      <p>If you are interested in participating in any of the aforementioned workshops or have an idea of one you would like to see then please register your interest.</p>
      <div class="text-right">
        <a href="#register-interest" class="btn btn-cta btn-cta-primary" data-toggle="modal"><i class="fa fa-fw fa-plus"></i> Register Interest</a>
      </div>
      <p>Our workshops are run by volunteers and are ran based on their availability and demand for a certain course - so let us know what you are interested in!</p>
    </div>
  </section>

  <!-- register interest modal -->
  <div class="modal modal-register-interest fade" id="register-interest" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="fa fa-times"></i></button>
          <h4 class="modal-title">Register Interest</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <form name="workshopVm.register" action="<%= url_for('register-interest') %>" method="post" class="form-signin" role="form">
                <input type="hidden" name="rt" value="<%= url_for('current') %>">
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
                    <input name="name" type="text" ng-model="workshopVm.name" class="form-control" placeholder="Name" required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-fw fa-envelope"></i></span>
                    <input name="email" type="email" ng-model="workshopVm.email" class="form-control" placeholder="Email" required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-fw fa-vote-yea"></i></span>
                    <select name="interest" class="form-control">
                      <option value="raspi-101">Raspberry Pi 101</option>
                      <option value="byo-drone">Build your own Drone/Quadcopter</option>
                      <option value="byo-3dp">Build your own 3D Printer</option>
                      <option value="iot-101">Internet of Things 101</option>
                      <option value="iot-201">Internet of Things 201</option>
                      <option value="arduino-101">Arduino 101</option>
                    </select>
                  </div>
                </div>
                <div>
                  <button class="btn btn-cta btn-cta-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-pencil" aria-hidden="true"></i> Cancel</button>
                  <button class="btn btn-cta btn-cta-primary pull-right" type="submit" ng-disabled="!workshopVm.canRegister()"><i class="fa fa-plus"></i> Register</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
