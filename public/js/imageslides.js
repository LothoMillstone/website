function setAttributes(el, attrs) {
  Object.keys(attrs).forEach(key => el.setAttribute(key, attrs[key]));
}

//bHack scroller
// Brett James, 2019

// https://api.flickr.com/services

let apiKey = "9d323adbdbc29938248d1f3e81d4e09d";
let method = "flickr.photosets.getPhotos";
let photoset_id = "72157642895664363";
let user_id = "23533984@N02";

let animationTypes = ["MOVE-BG", "MOVE-BG-REVERSE"];

const jsonFlickrApi = data => {
  // console.log(data);
  console.log(data.photoset);
  
  let base = document.getElementById("flickrImages");

  data.photoset.photo.forEach(element => {
    //console.log(element.title);
    // Make an element
    let newImage = document.createElement("img");
    let imageLocation = document.createAttribute("src");
    let imageSizes = {
      width: element.width_l / 2,
      height: element.height_l / 2
    };
    // element.title length != 0;
    imageLocation.value = element.url_l; // Set the value of the class attribute
    newImage.setAttributeNode(imageLocation);
    setAttributes(newImage, imageSizes);
    base.appendChild(newImage);
  });
};

function changePicture(pictures) {
  // let date = new Date();
  // let time = date.toLocaleTimeString();
  // document.getElementById('demo').textContent = time;
  // console.log("I've run.", pictures);

  var random = pictures[Math.floor(Math.random() * pictures.length)];
  var randomAnimation = animationTypes[Math.floor(Math.random() * animationTypes.length)];

  let oldImage = document.querySelector("#bg img");
  oldImage.parentNode.removeChild(oldImage);
  // console.log(pic.style)

  let newImage = document.createElement("img");
  // newImage.classList.add("fullpage-background");
  // console.log(newImage);
  newImage.src = random.url_l;
  // console.log(pic.src);
  // newImage.style.background = `url(${random.url_l})`;
  // newImage.style["animation-d"] = randomAnimation; // Was needed on class backgrounds for some reason
  // newImage.style["animation-name"] = randomAnimation;
  // let pic = (document.querySelector(".fullpage-background").outerHTML = "");
  // pic.outerHTML = "";
  document.querySelector("#bg").appendChild(newImage);
}

var timer = null;

const randomPicsApi = data => {
  console.log(data);
  console.log(data.photoset);

  // Only want landscape photos
  const suitableImages = data.photoset.photo.filter(
    data => data.width_l > 1000
  );
  console.log("We have", suitableImages);
  var random =
    suitableImages[Math.floor(Math.random() * suitableImages.length)];
  console.log(random);

  let pic = document.querySelector("#bg");
  // console.log(pic.style);
  // pic.style.background = `url(${random.url_l})`;
  

  // setInterval(function() {doStuff();},1000);
  timer = setInterval(function() {
    changePicture(suitableImages);
  }, 35000);

  // let base = document.getElementById("flickrImages");

  // data.photoset.photo.forEach((element) => {
  //   //console.log(element.title);
  //   // Make an element
  //   let newImage = document.createElement("img");
  //   let imageLocation = document.createAttribute("src");
  //   let imageSizes = {
  //     "width": (element.width_l / 2),
  //     "height": (element.height_l / 2)
  //   }
  //   // element.title length != 0;
  //   imageLocation.value = element.url_l;                           // Set the value of the class attribute
  //   newImage.setAttributeNode(imageLocation);
  //   setAttributes(newImage,imageSizes);
  //   base.appendChild(newImage);

  // } )
};

fetch(`https://www.flickr.com/services/rest/
?method=${method}
&format=json&nojsoncallback=1
&api_key=${apiKey}
&user_id=${user_id}
&photoset_id=${photoset_id}
&extras=url_l`)
  .then(response => response.json())
  .then(data => randomPicsApi(data));

// fetch('https://jsonplaceholder.typicode.com/users', {
//   headers: { "Content-Type": "application/json; charset=utf-8" },
//   method: 'POST',
//   body: JSON.stringify({
//     username: 'Elon Musk',
//     email: 'elonmusk@gmail.com',
//   })
// })
// .then(response => response.json())
// .then(data => console.log(data))

// fetch('https://jsonplaceholder.typicode.com/users', {
//   headers: { "Content-Type": "application/json; charset=utf-8" },
//   method: 'POST',
//   body: JSON.stringify({
//     api_key : "9d323adbdbc29938248d1f3e81d4e09d",
//     content: "flickr.test.echo",
//     gallery_id: "tesT"
// })
// })
// .then(response => response.json())
// .then(data => console.log(data))

// fetch('https://api.flickr.com/services', {
//   headers: { "Content-Type": "application/json; charset=utf-8" },
//   method: 'POST',
//   body: JSON.stringify({
//     api_key : "9d323adbdbc29938248d1f3e81d4e09d",
//     content: "flickr.test.echo",
//     gallery_id: 72157669118027867
// })
// })
// .then(response => response.json())
// .then(data => console.log(data))

// POST adds a random id to the object sent

// const request = async () => {
//     const response = await fetch('https://jsonplaceholder.typicode.com/users/2');
//     const json = await response.json();
//     console.log(json);
//     console.log("hello");
// }

// photoset id 72157669118027867
// const request = async () => {
//     let fetchData = {
//         method: "post",
//         headers: {
//           'Accept': 'application/json',
//           'Content-Type': 'application/json'
//         },

//         body: JSON.stringify({
//             api_key : "9d323adbdbc29938248d1f3e81d4e09d",
//             format:"JSON",
//             content: "flickr.test.echo",
//             gallery_id: "tesT"
//         })
//       }

//     const response = await fetch('https://api.flickr.com/services', fetchData);
//     const json = await response.json();
//     console.log(json);
// }

//request();
