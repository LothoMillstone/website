$(document).ready(function() {
  // initialise header animation
  $(window).on('scroll', function() {
    if ($(window).scrollTop() > 80) {
      $('#header').addClass('header-shrink');
    }
    else {
      $('#header').removeClass('header-shrink');
    }
  });

  // set minimum height
  var minHeight = $(window).height() - $('footer').height();
  $('.page').css('min-height', minHeight + 'px');
});

angular.module('bhack-core', ['ngStorage'])
  .config(['$localStorageProvider', function ($localStorageProvider) {
    $localStorageProvider.setKeyPrefix('BHack-');
  }])
  .factory('Device', function($localStorage) {
    var service = {
      assign: assign,
      getToken: getToken,
      getUUID: getUUID,
      isAssigned: isAssigned,
      unassign: unassign,
    };

    return service;

    ////////

    function assign(uuid, token) {
      $localStorage.deviceUUID = uuid;
      $localStorage.deviceToken = token;
    }

    function getToken() {
      return $localStorage.deviceToken;
    }

    function getUUID() {
      return $localStorage.deviceUUID;
    }

    function isAssigned() {
      return !!$localStorage.deviceUUID;
    }

    function unassign() {
      delete $localStorage.deviceUUID;
      delete $localStorage.deviceToken;
    }
  })
  .factory('Projects', function($http, $q) {
    var service = {
      get: get
    };

    return service;

    ////////

    function get(project) {
      var deferred = $q.defer();

      $http.get('/meta/projects/' + project + '/logs')
        .then(function(res) {
          deferred.resolve(res.data);
        }, function (err) {
          deferred.reject([]);
        });

      return deferred.promise;
    }
  })
  .factory('Streams', function($http, $q, $rootScope, $timeout) {
    var ws;
    var connected = false;

    var service = {
      getLastValue: getLastValue,
      history: history,
      isConnected: isConnected
    };

    activate();

    return service;

    ////////

    function activate() {
      connect();
    }

    function connect() {
      console.debug('WS: connecting ...');
      var url = (location.protocol === 'http:' ? 'ws://' : 'wss://') + location.hostname + ':' + location.port + '/meta/streams-ws';
      ws = new WebSocket(url);

      ws.onclose = function() {
        console.debug('WS: closed!');
        $timeout(function() {connected = false;}, 0);
        $timeout(connect, 10000);
        delete ws;
      };

      ws.onopen = function() {
        console.debug('WS: connected.');
        $timeout(function() {connected = true;}, 0);
      };

      ws.onerror = function() {
        console.debug('WS: error!');
      };

      ws.onmessage = function(msg) {
        var res = JSON.parse(msg.data);

        if (Object.keys(res).length > 0) {
          $timeout(function() {
            $rootScope.$broadcast('stream', res);
          }, 0);
        }
      };
    }

    function getLastValue(stream) {
      var deferred = $q.defer();

      history(stream, 1)
        .then(function(res) {
          if (res.data.hasOwnProperty('items') && angular.isArray(res.data.items) && res.data.items.length === 1) {
            deferred.resolve(res.data.items[0].data);
          } else {
            deferred.reject(undefined);
          }
        }, function(err) {
          deferreed.reject(undefined);
        });

      return deferred.promise;
    }

    function history(stream, total) {
      total = total || 10;

      return $http.get('/meta/streams/' + stream + '.json?fetch=' + total);
    }

    function isConnected() {
      return connected;
    }
  });

angular.module('bhack', ['bhack-core', 'gridster', 'ui.bootstrap'])
  .directive('bhGauge', function() {
    return {
      restrict: 'E',
      replace: 'true',
      template: '<div class="gauge"><canvas></canvas></div>',
      scope: {
        min: '=',
        max: '=',
        value: '@',
      },
      link: function(scope, el, attrs) {
        scope.min = scope.min || 0;
        scope.max = scope.max || 10;

        var opts = {
          angle: -0.15,
          lineWidth: 0.22,
          radiusScale: 0.8,
          pointer: {
            length: 0.6,
            strokeWidth: 0.035,
            color: '#3498db'
          },
          limitMax: true,
          limitMin: true,
          colorStart: '#2a8bcc40',
          colorStop: '#2a8bcc40',
          strokeColor: '#eee',
          generateGradient: true,
          highDpiSupport: true,     // High resolution support
        };

        var target = angular.element(el).find('canvas')[0];

        scope.$watch('min', updateGauge);
        scope.$watch('max', updateGauge);
        scope.$watch('value', updateGauge);
        scope.$watch(_watchVisibility, _onVisibilityChange);
        scope.$watch(_watchHeight, _onVisibilityChange);

        function setGauge() {
          scope.gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
          scope.gauge.animationSpeed = 16;
          scope.gauge.maxValue = scope.max;
          scope.gauge.setMinValue(scope.min);
          scope.gauge.set(scope.value);
        }

        function updateGauge(n, o) {
          var v = scope.value;
          if (scope.gauge) {
            scope.gauge.set(scope.value);
          }
        };

        function _onVisibilityChange(n) {
          if (n) {
            setGauge();
          }
        }

        function _watchHeight() {
          return el.height();
        }

        function _watchVisibility() {
          return angular.element(el).is(':visible');
        }
      }
    }
  })
  .directive('bhGraph', function() {
    return {
      restrict: 'E',
      replace: 'true',
      template: '<div class="graph"><canvas></canvas></div>',
      scope: {
        min: '=',
        max: '=',
        stream: '=',
      },
      link: function(scope, el, attrs) {
        var target = angular.element(el).find('canvas')[0];

        scope.$watch('stream', updateChart, true);
        scope.$watch(_watchVisibility, _onVisibilityChange);
        scope.$watch(_watchHeight, _onVisibilityChange);

        function setChart() {
          scope.chart = new Chart(target, {
            type: 'line',
            data: {
              labels: [],
              datasets: [{
                fill: false,
                backgroundColor: '#2a8bcc40',
                borderColor: '#2a8bcc40',
                pointBackgroundColor: '#3498db',
                pointBorderColor: '#3498db',
                data: [],
              }]
            },
            options: {
              responsive: true,
              maintainAspectRatio: false,
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  type: 'time',
                  time: {
                    tooltipFormat: 'YYYY-MM-DD HH:mm:ss',
                    parser: 'HH',
                  },
                  scaleLabel: {
                    display: false,
                  }
                }],
                yAxes: [{
                  scaleLabel: {
                    display: false,
                  }
                }]
              },
            }
          });
        }

        function updateChart(n, o) {
          if (scope.chart) {
            var l = Math.min(scope.stream.length, 32);
            var data = [];

            for (var i=l-1; i>=0; i--) {
              data.push({
                t: moment.unix(scope.stream[i].timestamp),
                y: scope.stream[i].data,
              });
            }

            scope.chart.data.datasets[0].data = data;
            scope.chart.update();
          }
        };

        function _onVisibilityChange(n) {
          if (n) {
            setChart();
            updateChart();
          }
        }

        function _watchHeight() {
          return el.height();
        }

        function _watchVisibility() {
          return angular.element(el).is(':visible');
        }
      }
    }
  })
  .directive('bhSlider', function() {
    return {
      restrict: 'E',
      replace: 'true',
      template: '<div class="slider"><div class="slider-track"><div class="slider-thumb"></div><div class="slider-level"></div></div><input class="slider-input" type="range" ng-model="slider" step="{{step}}" min="{{min}}" max="{{max}}"/></div>',
      scope: {
        min: '=',
        max: '=',
        step: '=',
        value: '@',
        onChange: '&',
      },
      link: function(scope, el, attrs) {
        var e = angular.element(el);
        var input = e.find('input')[0];
        var d = e.find('div');
        var thumb = d[1];
        var level = d[2];

        scope.min = scope.min || 0;
        scope.max = scope.max || 10;
        scope.step = scope.step || 1;

        scope.$watch('value', function(n, o) {
          if (!isNaN(n) && n !== o) {
            scope.slider = parseInt(n);
          }
        });

        scope.$watch('slider', function(n, o) {
          if (n !== o) {
            updateSliderLevel();
          }
        });

        e.bind('input', function() {
          if (scope.onChange) {
            scope.onChange({v: scope.slider});
          }
        });

        function updateSliderLevel() {
          var v = parseInt(scope.slider);
          var r = scope.max - scope.min;
          thumb.style.left = (v - scope.min) / r * 100 + '%';
          level.style.width = (v - scope.min) / r * 100 + '%';
        };
      }
    }
  })
  .directive('bhTime', function() {
    return {
      restrict: 'E',
      scope: { epoch: '@', format: '@?' },
      template: '<span>{{time()}}</span>',
      link: function(scope, el, attrs) {
        scope.time = function() {
          var m = moment.unix(scope.epoch);
          var f = scope.format || 'YYYY-MM-DD HH:mm:ss (Z)';
          return m.format(f);
        }
      }
    };
  })
  .directive('bhToggle', function() {
    return {
      restrict: 'E',
      replace: 'true',
      template: '<div class="toggle"><div class="btn-group"><button class="btn btn-cta btn-cta-green" ng-click="onButton(\'on\')" ng-disabled="state">{{onLabel}}</button><button class="btn btn-cta btn-cta-red" ng-click="onButton(\'off\')" ng-disabled="!state">{{offLabel}}</button></div></div>',
      scope: {
        offLabel: '@',
        offValue: '@',
        onLabel: '@',
        onValue: '@',
        onToggle: '&',
      },
      link: function(scope, el, attrs) {
        scope.state = false;

        scope.onLabel = scope.onLabel || 'On';
        scope.onValue = scope.onValue || 1;

        scope.offLabel = scope.offLabel || 'Off';
        scope.offValue = scope.offValue || 0;

        scope.onButton = function(btn) {
          scope.state = (btn === 'on');

          if (scope.onToggle) {
            scope.onToggle({v: scope.state ? scope.onValue : scope.offValue});
          }
        };
      }
    }
  })
  .directive("bhTooltip", function($timeout) {
    return {
      restrict: 'A',
      link: function(scope, el, attrs) {
        $timeout(function() {
          $(el).data('toggle', 'tooltip').data('title', attrs.bhTooltip).tooltip();
        });
      }
    }
  })
  .directive("bhWidget", function($http, $timeout) {
    var STREAM_HISTORY_MAX = 32;
    return {
      restrict: 'E',
      scope: { options: '=' },
      templateUrl: '/js/bhWidgets.tmpl.html',
      link: function(scope, el, attrs) {
        scope.stream = [];
        scope.updated = 0;

        scope.isType = function(type) {
          return type === scope.options.z;
        }

        scope.onButton = function() {
          scope.$emit('stream-out', {stream: scope.options.s, data: scope.options.a.p});
        }

        scope.onSlider = function(v) {
          scope.$emit('stream-out', {stream: scope.options.s, data: v});
        }

        scope.onToggle = function(v) {
          scope.$emit('stream-out', {stream: scope.options.s, data: v});
        }

        // fetch history
        $http.get('/meta/streams/' + scope.options.s + '.json?fetch=' + STREAM_HISTORY_MAX)
          .then(function(res) {
            if (res.data.hasOwnProperty('items') && res.data.items.length > 0) {
              // append array
              res.data.items.forEach(function(item) {
                scope.stream.push(item);
              });

              if (!scope.lastValue) {
                // fetching returns most recent value first
                scope.lastValue = res.data.items[0].data;
              }
            }
          }, function(err) {
            console.error('Unable to fetch history.');
          });


        // signals
        scope.$on('stream', function(e, message) {
          if (message.stream === scope.options.s) {
            scope.lastValue = message.data;
            scope.stream.unshift(message);

            // clamp stream length to max history length
            while (scope.stream.length > STREAM_HISTORY_MAX) {
              scope.stream.pop();
            }
            scope.updated = message.timestamp;
          }
        });
      }
    }
  })
  .controller('AdminBadgesController', function($http, $timeout, $window, $uibModal) {
    var vm = angular.extend(this, {
      badges: $window.bhack.badges.items,
    });


    angular.extend(this, {
      deleteBadge: deleteBadge,
      editBadge: editBadge,
      formatCategory: formatCategory,
    });

    ////////

    function deleteBadge(badge) {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalBadgeDelete.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, badge) {
          var $ctrl = this;
          $ctrl.badge = badge;
          $ctrl.ok = function() { $uibModalInstance.close(); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: { badge: function() { return badge; } }
      });

      modalInstance.result.then(function() {
        $http
          .post('/admin/badges/delete', {id: badge.id}, {'Content-type': 'application/json'})
          .then(function(data) {
            vm.badges = vm.badges.filter(function(e) { return e.id !== badge.id; });
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function editBadge(badge) {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalBadgeEdit.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, _badge) {
          var $ctrl = this;
          var badgeClean = angular.copy(_badge);
          $ctrl.badge = _badge;
          $ctrl.canSave = function() { return !angular.equals($ctrl.badge, badgeClean); };
          $ctrl.ok = function() { $uibModalInstance.close($ctrl.badge); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: { _badge: function() { return angular.copy(badge); } }
      });

      modalInstance.result.then(function(badgeUpdated) {
        $http
          .post('/admin/badges/update', badgeUpdated, {'Content-type': 'application/json'})
          .then(function(data) {
            var idx = vm.badges.findIndex(function(e) { return e.id === badge.id; });

            if (idx !== -1) {
              vm.badges[idx] = badgeUpdated;
            }
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function formatCategory(badge) {
      return badge.category || '-';
    }
  })
  .controller('AdminDevicesController', function($http, $window, $uibModal, Device) {
    var vm = angular.extend(this, {
      devices: $window.bhack.devices.items,
      member: $window.bhack.member,
    });

    angular.extend(this, {
      addDevice: addDevice,
      editDevice: editDevice,
      deleteDevice: deleteDevice,
      formatRole: formatRole,
      isDeviceCurrent: isDeviceCurrent,
    });

    var DEVICE_ROLES = {
      1:     'User',
      32768: 'Super User'
    };

    ////////

    function addDevice() {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalDeviceAdd.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, member) {
          var $ctrl = this;
          $ctrl.member = member
          $ctrl.device = {
            name: '',
            email: member.email,
            uuid: generateUUID(),
            token: generateUUID()
          };

          $ctrl.ok = function() { $uibModalInstance.close($ctrl.device); };
          $ctrl.canAdd = function() { return !!$ctrl.device.name.trim().length; };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };

          function generateUUID() {
            var d = new Date().getTime();
            if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
              d += performance.now(); //use high-precision timer if available
            }
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
              var r = (d + Math.random() * 16) % 16 | 0;
              d = Math.floor(d / 16);
              return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
          }
        },
        resolve: {member: function() { return vm.member; }},
        size: 'md',
      });

      modalInstance.result.then(function(device) {
        $http
          .post('/members/devices/add', device, {'Content-type': 'application/json'})
          .then(function(data) {
            device.created = moment().unix();
            vm.devices.push(device);
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function deleteDevice(device) {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalDeviceDelete.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, device) {
          var $ctrl = this;
          $ctrl.device = device;
          $ctrl.ok = function() { $uibModalInstance.close(); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: {device: function() { return device; }}
      });

      modalInstance.result.then(function() {
        $http
          .post('/members/devices/delete', {uuid: device.uuid}, {'Content-type': 'application/json'})
          .then(function(data) {
            vm.devices = vm.devices.filter(function(e) { return e.uuid !== device.uuid; });
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function editDevice(device) {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalDeviceEdit.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, _device) {
          var $ctrl = this;
          var deviceClean = angular.copy(_device);
          $ctrl.device = _device;

          $ctrl.ok = function() { $uibModalInstance.close($ctrl.device); };
          $ctrl.canSave = function() { return !angular.equals($ctrl.device, deviceClean); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        resolve: {_device: function() { return device; }},
        size: 'md',
      });

      modalInstance.result.then(function(deviceUpdated) {
        $http
          .post('/members/devices/update', deviceUpdated, {'Content-type': 'application/json'})
          .then(function(data) {
            var idx = vm.devices.findIndex(function(e) { return e.id === device.id; });

            if (idx !== -1) {
              vm.devices[idx] = deviceUpdated;
            }
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }
    function formatRole(role) {
      if (DEVICE_ROLES.hasOwnProperty(role)) {
        return DEVICE_ROLES[role];
      }

      return '???';
    }

    function isDeviceCurrent(device) {
      return Device.isAssigned() && device.uuid === Device.getUUID();
    }
  })
  .controller('AdminStreamsController', function($http, $scope, $timeout, $window, $uibModal, Streams) {
    var vm = angular.extend(this, {
      streams: $window.bhack.streams.items,
      streamsAcls: $window.bhack.streamsAcls.items,
    });

    angular.extend(this, {
      addAcl: addAcl,
      deleteAcl: deleteAcl,
      deleteStream: deleteStream,
      editAcl: editAcl,
      fetchMore: fetchMore,
      formatAclAccess: formatAclAccess,
      formatConnectedState: formatConnectedState,
      isConnected: isConnected,
    });

    $scope.$on('stream', _onStream);

    activate();

    ////////

    function activate() {
      // initial fetch of at least 10 items
      vm.relative = !!$window.bhack.streams.relative ? $window.bhack.streams.relative : [];
      vm.showMore = vm.relative.length > 0 && vm.streams.length >= 10;
    }

    function addAcl() {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalAclCreate.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance) {
          var $ctrl = angular.extend(this, {
            acl: {
              uuid: '',
              topic: '',
              access: "1"
            },
            canAdd: function() {
              return $ctrl.acl.uuid.length > 0 &&
                $ctrl.acl.topic.length > 0;
              ;
            },
            cancel: function() { $uibModalInstance.dismiss('cancel'); },
            ok: function() {
              $ctrl.acl.access = parseInt($ctrl.acl.access);
              $uibModalInstance.close($ctrl.acl);
            },
          });
        },
        size: 'md',
      });

      modalInstance.result.then(function(acl) {
        $http
          .post('/admin/streams/acl', acl, {'Content-type': 'application/json'})
          .then(function(data) {
            vm.streamsAcls.push({
              timestamp: moment().unix(),
              uuid: acl.uuid,
              topic: acl.topic,
              access: acl.access,
            });
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function deleteAcl(acl) {
      console.log('woot');
      var modalInstance = $uibModal.open({
        templateUrl: 'modalAclDelete.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, _acl) {
          var $ctrl = angular.extend(this, {
            acl: _acl,
            ok: function() { $uibModalInstance.close(); },
            cancel: function() { $uibModalInstance.dismiss('cancel'); },
          });
        },
        size: 'md',
        resolve: { _acl: function() { return acl; } }
      });

      modalInstance.result.then(function() {
        $http
          .post('/admin/streams/acl/delete', acl, {'Content-type': 'application/json'})
          .then(function(data) {
            vm.streamsAcls = vm.streamsAcls.filter(function(e) {
              return e.uuid !== acl.uuid || e.topic !== acl.topic;
            });
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function deleteStream(stream) {
      var deleteAll = (stream.hasOwnProperty('stream') && stream.stream === '*');

      var modalInstance = $uibModal.open({
        templateUrl: deleteAll ? 'modalStreamDeleteAll.html' : 'modalStreamDelete.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, _stream) {
          var $ctrl = angular.extend(this, {
            stream: _stream,
            ok: function() { $uibModalInstance.close(); },
            cancel: function() { $uibModalInstance.dismiss('cancel'); },
          });
        },
        size: 'md',
        resolve: { _stream: function() { return stream; } }
      });

      modalInstance.result.then(function() {
        $http
          .post('/admin/streams/delete', {stream: stream.stream}, {'Content-type': 'application/json'})
          .then(function(data) {
            vm.streams = vm.streams.filter(function(e) { return e.stream !== stream.stream; });
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function editAcl(acl) {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalAclEdit.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, _acl) {
          var _aclOrig = {
            id: _acl.id,
            uuid: _acl.uuid,
            topic: _acl.topic,
            access: String(_acl.access),
          };

          var $ctrl = angular.extend(this, {
            acl: {
              id: _acl.id,
              uuid: _acl.uuid,
              topic: _acl.topic,
              access: String(_acl.access),
            },
            canSave: function() {
              return !angular.equals($ctrl.acl, _aclOrig) &&
                $ctrl.acl.uuid.length > 0 &&
                $ctrl.acl.topic.length > 0;
              ;
            },
            cancel: function() {
              $uibModalInstance.dismiss('cancel');
            },
            ok: function() {
              $ctrl.acl.access = parseInt($ctrl.acl.access);
              $uibModalInstance.close($ctrl.acl);
            },
          });

        },
        size: 'md',
        resolve: { _acl: function() { return acl; } }
      });

      modalInstance.result.then(function(acl) {
        $http
          .post('/admin/streams/acl/update', acl, {'Content-type': 'application/json'})
          .then(function(data) {
            var i = vm.streamsAcls.findIndex(function(e) { return e.id === acl.id });
            if (i !== -1) {
              vm.streamsAcls[i] = angular.merge({}, vm.streamsAcls[i], acl);
            }
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function fetchMore() {
      $http.get('/meta/streams/' + vm.name + '.json?relative=' + vm.relative)
        .then(function(res) {
          // append our new results
          vm.stream.push.apply(vm.stream, res.data.items);

          // update fetch more
          vm.relative = res.data.relative;
          vm.showMore = !!vm.relative && res.data.items.length >= 10;
        }, function(err) {
          console.error(err);
        });
    }

    function formatAclAccess(acl) {
      if (acl.hasOwnProperty('access')) {
        if (acl.access === 1) {
          return 'R';
        } else if (acl.access === 2) {
          return 'W';
        } else if (acl.access === 3) {
          return 'RW';
        }
      }

      return '?';
    }

    function formatConnectedState() {
      return isConnected() ? 'Online' : 'Offline';
    }

    function isConnected() {
      return Streams.isConnected();
    }

    // PRIVATE

    function _onStream(e, message) {
      var idx = vm.streams.findIndex(function(e) { return e.stream === message.stream; });

      if (idx === -1) {
        vm.streams.unshift({
          stream: message.stream,
          timestamp: message.timestamp,
          points: 1
        });
      } else {
        vm.streams[idx].points++;
      }
    }

  })
  .controller('AdminUsersController', function($http, $window) {
    var vm = angular.extend(this, {
    });

    angular.extend(this, {
    });

    ////////

  })
  .controller('LoginController', function($http, $location) {
    var vm = angular.extend(this, {
      loggedIn: false,
      working: false,
      email: '',
      password: '',
    });

    angular.extend(this, {
      canForget: canForget,
      canLogin: canLogin,
      getForgetUrl: getForgetUrl,
      login: login
    });

    activate();

    ////////

    function activate() {
      // autofocus email input on modal show
      $('#login').on('shown.bs.modal', function() {
        $('input[name=email]').focus();
      })
    }

    function canForget() {
      return !vm.working && (vm.email && vm.email.length > 3);
    }

    function getForgetUrl() {
      if (canForget()) {
        return 'https://accounts.tidyhq.com/users/password?email=' + encodeURIComponent(vm.email);
      }

      return '#';
    }

    function canLogin() {
      return !vm.working &&
        (vm.email && vm.email.length > 3) &&
        (vm.password && vm.password.length > 0);
    }

    function login() {
      vm.working = true;

      var data = {
        email: vm.email,
        password: vm.password,
      };

      var req = {
        method: 'POST',
        url: '/login',
        headers: { 'Content-type': 'application/json' },
        data: data
      }

      $http(req).then(function(data){
        window.location.href = '/members/profile/me';
      }, function(err) {
        vm.working = false;
        console.error(err);
      });
    }
  })
  .controller('ProfileController', function($http, $location, $window, $uibModal, Device, Projects) {
    var vm = angular.extend(this, {
      devices: $window.bhack.devices.items,
      projects: $window.bhack.projects.items,
      member: $window.bhack.member,
    });

    angular.extend(this, {
      addDevice: addDevice,
      addProject: addProject,
      deleteDevice: deleteDevice,
      deleteProject: deleteProject,
      editProject: editProject,
      formatDeviceUUID: formatDeviceUUID,
      hasDevices: hasDevices,
      hasProjects: hasProjects,
      isDeviceCurrent: isDeviceCurrent,
      isRegistered: isRegistered,
      toggleDeviceAssignment: toggleDeviceAssignment,
    });

    ////////

    function addDevice() {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalDeviceAdd.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, member) {
          var $ctrl = this;
          $ctrl.member = member
          $ctrl.device = {
            name: '',
            email: member.email,
            uuid: generateUUID(),
            token: generateUUID()
          };

          $ctrl.ok = function() { $uibModalInstance.close($ctrl.device); };
          $ctrl.canAdd = function() { return !!$ctrl.device.name.trim().length; };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };

          function generateUUID() {
            var d = new Date().getTime();
            if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
              d += performance.now(); //use high-precision timer if available
            }
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
              var r = (d + Math.random() * 16) % 16 | 0;
              d = Math.floor(d / 16);
              return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
          }
        },
        resolve: {member: function() { return vm.member; }},
        size: 'md',
      });

      modalInstance.result.then(function(device) {
        $http
          .post('/members/devices/add', device, {'Content-type': 'application/json'})
          .then(function(data) {
            device.created = moment().unix();
            vm.devices.push(device);
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function addProject() {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalProjectAdd.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, member) {
          var EMPTY_LOG = {
            data: {
              title: '',
              image_urls: [''],
              entry: '',
            }
          };

          var $ctrl = this;
          $ctrl.member = member
          $ctrl.project = {
            title: '',
            description: '',
            image_url: '',
            email: member.email,
            logs: [angular.copy(EMPTY_LOG)],
            meta: {
              tags: ''
            },
          };

          $ctrl.addLog = function(index) {
            $ctrl.project.logs.splice(index, 0, angular.copy(EMPTY_LOG));
          };
          $ctrl.canAdd = function() { return !!$ctrl.project.title.trim().length; };
          $ctrl.clearLog = function(index) {
            if ($ctrl.project.logs.length > 1) {
              $ctrl.project.logs.splice(index, 1);
            } else {
              $ctrl.project.logs = [angular.copy(EMPTY_LOG)];
            }
          }
          $ctrl.isLogFirst = function(index) {
            return index === 0;
          }
          $ctrl.isLogLast = function(index) {
            return index === $ctrl.project.logs.length - 1;
          }
          $ctrl.lowerLog = function(index) {
            if (!$ctrl.isLogLast(index)) {
              [$ctrl.project.logs[index], $ctrl.project.logs[index+1]] = [$ctrl.project.logs[index+1], $ctrl.project.logs[index]];
            }
          };
          $ctrl.raiseLog = function(index) {
            if (!$ctrl.isLogFirst(index)) {
              [$ctrl.project.logs[index], $ctrl.project.logs[index-1]] = [$ctrl.project.logs[index-1], $ctrl.project.logs[index]];
            }
          };

          $ctrl.ok = function() {
            $ctrl.project.meta.tags = $ctrl.project.meta.tags.split(/\s*,\s*/);
            // filter empty logs
            $ctrl.project.logs = $ctrl.project.logs.filter(function(log) {
              return !angular.equals(log, EMPTY_LOG);
            });
            $uibModalInstance.close($ctrl.project);
          };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        resolve: {member: function() { return vm.member; }},
        size: 'md',
      });

      modalInstance.result.then(function(project) {
        $http
          .post('/members/projects/add', project, {'Content-type': 'application/json'})
          .then(function(data) {
            project.created = moment().unix();
            project.stub = _sanitiseStub(project.title);
            vm.projects.push(project);
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function deleteDevice(device) {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalDeviceDelete.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, device) {
          var $ctrl = this;
          $ctrl.device = device;
          $ctrl.ok = function() { $uibModalInstance.close(); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: { device: function() { return device; } }
      });

      modalInstance.result.then(function() {
        $http
          .post('/members/devices/delete', {uuid: device.uuid}, {'Content-type': 'application/json'})
          .then(function(data) {
            vm.devices = vm.devices.filter(function(e) { return e.uuid !== device.uuid; });
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function deleteProject(project) {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalProjectDelete.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, _project) {
          var $ctrl = this;
          $ctrl.project = _project;
          $ctrl.ok = function() { $uibModalInstance.close(); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: { _project: function() { return project; } }
      });

      modalInstance.result.then(function() {
        $http
          .post('/members/projects/delete', {stub: project.stub}, {'Content-type': 'application/json'})
          .then(function(data) {
            vm.projects = vm.projects.filter(function(e) { return e.stub !== project.stub; });
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function editProject(project) {
      var modalInstance = $uibModal.open({
        templateUrl: 'modalProjectEdit.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, logs, member, _project) {
            var EMPTY_LOG = {
              data: {
                title: '',
                image_urls: [''],
                entry: '',
              }
            };

          _project.meta.tags = angular.isArray(_project.meta.tags) ? _project.meta.tags.join(', ') : '';
          _project.logs = logs.length > 0 ? logs : [angular.copy(EMPTY_LOG)];

          var $ctrl = this;
          $ctrl.member = member
          $ctrl.project = _project;
          var projectClean = angular.copy(_project);

          $ctrl.addLog = function(index) {
            _project.logs.splice(index, 0, angular.copy(EMPTY_LOG));
          };
          $ctrl.canSave = function() { return !angular.equals($ctrl.project, projectClean); };
          $ctrl.clearLog = function(index) {
            if (_project.logs.length > 1) {
              _project.logs.splice(index, 1);
            } else {
              _project.logs = [angular.copy(EMPTY_LOG)];
            }
          }
          $ctrl.isLogFirst = function(index) {
            return index === 0;
          }
          $ctrl.isLogLast = function(index) {
            return index === _project.logs.length - 1;
          }
          $ctrl.lowerLog = function(index) {
            if (!$ctrl.isLogLast(index)) {
              [_project.logs[index], _project.logs[index+1]] = [_project.logs[index+1], _project.logs[index]];
            }
          };
          $ctrl.raiseLog = function(index) {
            if (!$ctrl.isLogFirst(index)) {
              [_project.logs[index], _project.logs[index-1]] = [_project.logs[index-1], _project.logs[index]];
            }
          };
          $ctrl.ok = function() {
            $ctrl.project.meta.tags = $ctrl.project.meta.tags.split(/\s*,\s*/);
            // filter empty logs
            $ctrl.project.logs = _project.logs.filter(function(log) {
              return !angular.equals(log, EMPTY_LOG);
            });
            $uibModalInstance.close($ctrl.project);
          };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: {
          logs: function() { return Projects.get(project.stub); },
          member: function() { return vm.member; },
          _project: function() { return angular.copy(project); }
        },
      });

      modalInstance.result.then(function(projectUpdated) {
        $http
          .post('/members/projects/update', projectUpdated, {'Content-type': 'application/json'})
          .then(function(data) {
            var idx = vm.projects.findIndex(function(e) { return e.id === project.id; });

            if (idx !== -1) {
              vm.projects[idx] = projectUpdated;
            }
          }, function(err) {
            console.error(err);
          });
      }, function() {
        // modal cancelled
      });
    }

    function formatDeviceUUID() {
      if (Device.isAssigned()) {
        return Device.getUUID();
      }

      return 'Not set.';
    }

    function hasDevices() {
      return vm.devices.length > 0;
    }

    function hasProjects() {
      return vm.projects.length > 0;
    }

    function isDeviceCurrent(device) {
      return Device.isAssigned() && device.uuid === Device.getUUID();
    }

    function isRegistered() {
      if (Device.isAssigned()) {
        var uuid = Device.getUUID();
 
        return vm.devices.findIndex(function(e) { return e.uuid === uuid}) !== -1;
      }

      return false;
    }

    function toggleDeviceAssignment(device) {
      if (isDeviceCurrent(device)) {
        Device.unassign();
      } else {
        Device.assign(device.uuid, device.token);
      }
    }

    // PRIVATE

    function _sanitiseStub(stub) {
      var s = stub.trim().toLowerCase();

      s.replace(/&.+?;/g, '');
      s.replace(/\./g, '-');
      s.replace(/[^%a-z0-9 _-]/g, '');
      s.replace(/\s+/g, '-');
      s.replace(/-+/g, '-');
      s.replace(/-+$/g, '');

      return s;
    }
  })
  .controller('StreamController', function($http, $scope, $timeout, $window, Streams) {
    var vm = angular.extend(this, {
      name: $window.bhack.name,
      stream: $window.bhack.stream.items,
      showMore: false,
    });

    angular.extend(this, {
      fetchMore: fetchMore,
      formatConnectedState: formatConnectedState,
      isConnected: isConnected,
    });

    $scope.$on('stream', _onStream);

    activate();

    ////////

    function activate() {
      // initial fetch of at least 10 items
      vm.relative = !!$window.bhack.stream.relative ? $window.bhack.stream.relative : undefined;
      vm.showMore = !!vm.relative && vm.stream.length >= 10;
    }

    function fetchMore() {
      $http.get('/meta/streams/' + vm.name + '.json?relative=' + vm.relative)
        .then(function(res) {
          // append our new results
          vm.stream.push.apply(vm.stream, res.data.items);

          // update fetch more
          vm.relative = res.data.relative;
          vm.showMore = !!vm.relative && res.data.items.length >= 10;
        }, function(err) {
          console.error(err);
        });
    }

    function formatConnectedState() {
      return isConnected() ? 'Online' : 'Offline';
    }

    function isConnected() {
      return Streams.isConnected();
    }

    // PRIVATE

    function _onStream(e, message) {

      if (message.stream === vm.name) {
        vm.stream.unshift({
          data: message.data,
          timestamp: message.timestamp,
        });
      }
    }
  })
  .controller('StreamDashboardController', function($http, $scope, $timeout, $uibModal, $window, Streams) {
    var WIDGET_MAX_WIDTH = 12;

    var vm = angular.extend(this, {
      items: [],
      options: {
        columns: WIDGET_MAX_WIDTH,
        draggable: { enabled: false, },
        floating: false,
        margins: [8, 8],
        mobileBreakPoint: 719,
        outerMargin: false,
        pushing: true,
        resizable: { enabled: false},
        swapping: true,
      },
      modeEdit: false,
    });

    angular.extend(this, {
      addWidget: addWidget,
      bookmark: bookmark,
      canEdit: canEdit,
      deleteWidget: deleteWidget,
      editWidget: editWidget,
      formatConnectedState: formatConnectedState,
      hasWidgets: hasWidgets,
      isConnected: isConnected,
      toggleMode: toggleMode,
    });

    $scope.$on('stream-out', _onStreamOut);

    activate();

    ////////

    function activate() {
      // strip angular prefix if present
      var b = $window.location.hash.replace(/^#!#/, '');

      $timeout(function() {
        try {
          vm.items = JSON.parse($window.atob(b));
        } catch (e) {
          vm.items = [];
        }
      }, 0);
    }

    function addWidget(type) {
      var _type = type || 'button';

      var modalInstance = $uibModal.open({
        templateUrl: 'modalWidgetAdd.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, type) {
          var $ctrl = this;
          $ctrl.args = {
            button: {
              p: 1,      // pressed value
              P: 'Stop', // pressed label
            },
            graph: {
              m: 0,   // min
              x: 50,  // max
            },
            gauge: {
              m: 0,   // min
              x: 50,  // max
            },
            slider: {
              m: 0,   // min
              x: 50,  // max
              s: 1,   // step
            },
            stream: {},
            toggle: {
              f: 0,     // off value
              F: 'Off', // off label
              o: 1,     // on value
              O: 'On',  // on label
            },
          };
          $ctrl.widget = {
            title: Math.random().toString(36).substr(2),
            type: type,
            stream: '',
            args: $ctrl.args[type]
          };
          $ctrl.canAdd = function() { return $ctrl.widget.stream !== ''; };
          $ctrl.hasArg = function(a) { return $ctrl.widget.args.hasOwnProperty(a); };
          $ctrl.updateType = function() {
            $ctrl.widget.args = $ctrl.args[$ctrl.widget.type];
          };
          $ctrl.ok = function() { $uibModalInstance.close($ctrl.widget); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: { type: function() { return _type; } }
      });

      modalInstance.result.then(function(widget) {
        var y = Math.max.apply(Math, vm.items.map(function(o) { return o.y + o.h; }));
        var w = {
          x: 0,             // x position (col)
          y: y,             // y position (row)
          w: 3,             // width
          h: 3,             // height
          z: widget.type,   // type
          t: widget.title,  // title
          s: widget.stream, // stream
          a: widget.args,   // type specific args
        }
        vm.items.push(w);
      }, function() {
        // modal cancelled
      });
    }

    function bookmark() {
      // strip angular members if present
      var o = angular.copy(vm.items);
      $window.location.hash = $window.btoa(JSON.stringify(o));
    }

    function canEdit() {
      return vm.modeEdit;
    }

    function deleteWidget(index) {
      var widget = vm.items[index];

      var modalInstance = $uibModal.open({
        templateUrl: 'modalWidgetDelete.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, widget) {
          var $ctrl = this;
          $ctrl.widget = widget;
          $ctrl.ok = function() { $uibModalInstance.close(); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: { widget: function() { return widget; } }
      });

      modalInstance.result.then(function() {
        vm.items.splice(index, 1);
      }, function() {
        // modal cancelled
      });
    }

    function editWidget(index) {
      var _widget = vm.items[index];

      var modalInstance = $uibModal.open({
        templateUrl: 'modalWidgetEdit.html',
        controllerAs: 'modalVm',
        controller: function($uibModalInstance, widget) {
          var $ctrl = this;
          $ctrl.args = {
            button: {
              p: 1,      // pressed value
              P: 'Stop', // pressed label
            },
            graph: {
              m: 0,   // min
              x: 50,  // max
            },
            gauge: {
              m: 0,   // min
              x: 50,  // max
            },
            slider: {
              m: 0,   // min
              x: 50,  // max
              s: 1,   // step
            },
            stream: {},
            toggle: {
              f: 0,     // off value
              F: 'Off', // off label
              o: 1,     // on value
              O: 'On',  // on label
            },
          };

          $ctrl.widget = angular.merge(
            {
              args: $ctrl.args[widget.z]
            },
            {
              x: widget.x,
              y: widget.y,
              width: widget.w,
              height: widget.h,
              type: widget.z,
              title: widget.t,
              stream: widget.s,
              args: widget.a,
            }
          );

          $ctrl.canUpdate = function() { return $ctrl.widget.stream !== ''; };
          $ctrl.hasArg = function(a) { return $ctrl.widget.args.hasOwnProperty(a); };
          $ctrl.ok = function() { $uibModalInstance.close($ctrl.widget); };
          $ctrl.cancel = function() { $uibModalInstance.dismiss('cancel'); };
        },
        size: 'md',
        resolve: { widget: function() { return _widget; } }
      });

      modalInstance.result.then(function(widget) {
        var w = {
          x: widget.x,        // x position (col)
          y: widget.y,        // y position (row)
          w: widget.width,    // width
          h: widget.height,   // height
          z: widget.type,     // type
          t: widget.title,    // title
          s: widget.stream,   // stream
          a: widget.args,     // type specific args
        };

        vm.items[index] = w;
      }, function() {
        // modal cancelled
      });
    }

    function formatConnectedState() {
      return isConnected() ? 'Online' : 'Offline';
    }

    function hasWidgets() {
      return vm.items.length > 0;
    }

    function isConnected() {
      return Streams.isConnected();
    }

    function toggleMode() {
      vm.modeEdit = !vm.modeEdit;

      vm.options.draggable.enabled = vm.modeEdit;
      vm.options.resizable.enabled = vm.modeEdit;

      if (!vm.modeEdit && hasWidgets()) {
        bookmark();
      }
    }

    // PRIVATE

    function _onStreamOut(e, message) {
      if (vm.ws) {
        // TODO: rate limit?
        vm.ws.send(JSON.stringify(message));
      }
    }
  })
  .controller('StreamsController', function($http, $scope, $timeout, $window, Streams) {
    var vm = angular.extend(this, {
      streams: $window.bhack.streams.items,
    });

    angular.extend(this, {
      formatConnectedState: formatConnectedState,
      isConnected: isConnected,
      hasStreams: hasStreams,
    });

    $scope.$on('stream', _onStream);

    activate();

    ////////

    function activate() {
    }

    function formatConnectedState() {
      return isConnected() ? 'Online' : 'Offline';
    }

    function hasStreams() {
      return vm.streams.length > 0;
    }

    function isConnected() {
      return Streams.isConnected();
    }

    // PRIVATE

    function _onStream(e, message) {
      vm.streams.unshift(message);

      // truncate size to 32 entries
      while (vm.streams.length > 32) {
        vm.streams.pop()
      }
    }
  })
  .controller('WorkshopController', function($http, $window, $uibModal) {
    var vm = angular.extend(this, {
      name: '',
      email: '',
    });

    angular.extend(this, {
      canRegister: canRegister,
    });

    ////////

    function canRegister() {
      return vm.register.name.$valid && vm.register.email.$valid;
    }
  });

angular.module('bhack-door', ['bhack-core'])
  .controller('DoorController', function($scope, $http, $timeout, Device, Streams) {
    var vm = angular.extend(this, {
      errors: '',
      uuid: Device.getUUID(),
      token: Device.getToken(),
      latchOpen: false,
      showUnlock: false,
      solenoidOpen: false,
      unlocking: false,
    });

    angular.extend(this, {
      canUnlock: canUnlock,
      hasErrors: hasErrors,
      isAssigned: isAssigned,
      isOpen: isOpen,
      isUnlocked: isUnlocked,
      isWorking: isWorking,
      unlock: unlock,
    });

    $scope.$on('stream', _onStream);

    activate();

    ////////

    function activate() {
      Streams.getLastValue('monitor/door/latch')
        .then(function(value) {
          vm.latchOpen = (value === 1);
        });

      Streams.getLastValue('monitor/door/solenoid')
        .then(function(value) {
          vm.solenoidOpen = (value === 1);
        });
    }

    function canUnlock() {
      return !isUnlocked() && !isWorking() && Streams.isConnected();
    }

    function hasErrors() {
      return !!vm.errors && vm.errors.length > 0;
    }

    function isAssigned(device) {
      return Device.isAssigned();
    }

    function isOpen() {
      return vm.latchOpen;
    }

    function isUnlocked() {
      return vm.solenoidOpen && !isWorking();
    }

    function isWorking() {
      return vm.requestTimer !== undefined;
    }

    function unlock() {
      if (!canUnlock()) {
        return;
      }

      _animate('.padlock', 'pulse');

      // clear errors
      vm.errors = '';
      vm.unlocking = true;

      vm.requestTimer = $timeout(function() {
        vm.errors = 'Request timed out!';
        vm.unlocking = false;
        vm.requestUnlock = false;
        vm.requestTimer = undefined;

        _animate('.padlock', 'wobble');
      }, 5000);

      $http.post('/door/enter', {uuid: vm.uuid, token: vm.token}, {'Content-type': 'application/json'})
        .then(function(res) {
        }, function(err) {
          $timeout.cancel(vm.requestTimer);
          vm.requestTimer = undefined;

          if (err.hasOwnProperty('data') && err.data.hasOwnProperty('error')) {
            vm.errors = err.data.error;
          } else {
            vm.errors = 'Unknown error!';
            console.error(err);
          }

          _animate('.padlock', 'wobble');
        })
      .finally(function() {
        vm.unlocking = false;
      });
    }

    /*
    ** PRIVATE
    */

    function _animate(selector, type) {
      $(selector).addClass(type + ' animated')
        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
          $(this).removeClass(type + ' animated');
        });
    }

    function _onStream(e, message) {
      console.debug(e, message);

      if (message.stream === 'monitor/door/solenoid') {
        vm.solenoidOpen = !!parseInt(message.data);

        if (isWorking()) {
          $timeout.cancel(vm.requestTimer);
          vm.requestTimer = undefined;
        }

        if (vm.solenoidOpen && hasErrors()) {
          vm.errors = '';
        }
      } else if (message.stream === 'monitor/door/latch') {
        vm.latchOpen = (parseInt(message.data) === 1);
      }
    }
  });
